const mix = require('laravel-mix')

require('vuetifyjs-mix-extension')

mix
.js('resources/js/app.js', 'public/js')
.postCss('resources/css/app.css', 'public/css', [
    require('postcss-import'),
    require('tailwindcss'),
])
.webpackConfig(require('./webpack.config'))
.vuetify();